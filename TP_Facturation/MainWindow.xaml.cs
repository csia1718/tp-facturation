﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Web;
using System.IO;
using MySql.Data.MySqlClient;
using MahApps.Metro.Controls;

namespace TP_Facturation
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        Cnx_bdd maConnexion = new Cnx_bdd();
        List<Client> listClient = new List<Client>();
        Boolean bModif = false;
        String idClient; 

        public MainWindow()
        {
            InitializeComponent();
            refreshClient();
        }

        private void btnValider_Click(object sender, RoutedEventArgs e)
        {
            if (bModif == true)
            {
                maConnexion.updateClient(idClient, txtNom.Text, txtPrenom.Text, txtNumero.Text, txtAdresse.Text);

                txtAdresse.Text = "";
                txtNom.Text = "";
                txtNumero.Text = "";
                txtPrenom.Text = "";

                bModif = false;
                idClient = "";
                refreshClient();
            }
            else
            {
                maConnexion.addClient(txtNom.Text, txtPrenom.Text, txtNumero.Text, txtAdresse.Text);
                refreshClient();

                txtAdresse.Text = "";
                txtNom.Text = "";
                txtNumero.Text = "";
                txtPrenom.Text = "";
            }

        }

        private void btnModifier_Click(object sender, RoutedEventArgs e)
        {
            int indexTable = dgvInfos.SelectedIndex;
            if (indexTable != -1)
            {
                Client monClient = new Client();
                monClient = (Client)dgvInfos.SelectedItem;

                txtNom.Text = monClient.Nom;
                txtPrenom.Text = monClient.Prenom;
                txtNumero.Text = monClient.Numero;
                txtAdresse.Text = monClient.Adresse;
                btnAnnuler.Visibility = Visibility.Visible;
                idClient = monClient.Id;
                bModif = true;
            }
        }

        private void btnSupprimer_Click(object sender, RoutedEventArgs e)
        {

            int indexTable = dgvInfos.SelectedIndex;
            if(indexTable != -1)
            {
                Client monClient = new Client();
                monClient = (Client)dgvInfos.SelectedItem;
                maConnexion.deleteClient(monClient.Id.ToString());
            }

            refreshClient();

        }

        private void refreshClient()
        {
            listClient = maConnexion.getAllClient();
            dgvInfos.ItemsSource = listClient;
            dgvInfos.Items.Refresh();
        }

        private void btnAnnuler_Click(object sender, RoutedEventArgs e)
        {
            txtAdresse.Text = "";
            txtNom.Text = "";
            txtNumero.Text = "";
            txtPrenom.Text = "";

            bModif = false;
            refreshClient();

            btnAnnuler.Visibility = Visibility.Hidden;
        }
    }
}
