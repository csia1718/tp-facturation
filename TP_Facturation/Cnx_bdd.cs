﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using TP_Facturation;

namespace TP_Facturation
{
    class Cnx_bdd
    {
        private static string connectionString = "SERVER=127.0.0.1; DATABASE=tp_facturation; UID=root; PASSWORD=;";
        private static MySqlConnection connection = new MySqlConnection(connectionString);

        public List<Client> getAllClient()
        {
            List<Client> listClient = new List<Client>();
            string queryString = "SELECT * FROM client";
            MySqlCommand command = new MySqlCommand(queryString, connection);
            this.openConnect();
            MySqlDataReader oReader = command.ExecuteReader();

            while (oReader.Read())
            {
                Client newClient = new Client();
                newClient.Id = oReader["ID"].ToString();
                newClient.Nom = oReader["NOM"].ToString();
                newClient.Prenom = oReader["PRENOM"].ToString();
                newClient.Numero = oReader["TELEPHONE"].ToString();
                newClient.Adresse = oReader["ADRESSE"].ToString();
                listClient.Add(newClient);
            }
            this.closeConnect();

            return listClient;
        }

        public void addClient(String NOM, String PRENOM, String TELEPHONE, String ADRESSE)
        {
            Guid ID = Guid.NewGuid();
            this.openConnect();
            string param = "('" + ID + "','" + NOM + "','" + PRENOM + "','" + TELEPHONE  + "','" + ADRESSE + "')";
            string queryString = "INSERT INTO client (ID,NOM,PRENOM,TELEPHONE,ADRESSE) VALUES " + param;
            MySqlCommand command = new MySqlCommand(queryString, connection);
            command.ExecuteNonQuery();
            this.closeConnect();
        }

        public void updateClient(String ID, String NOM, String PRENOM, String TELEPHONE, String ADRESSE)
        {
            this.openConnect();
            string where = "WHERE ID = '" + ID + "'";
            string queryString = "UPDATE client SET NOM = '" + NOM + "', PRENOM = '" + PRENOM + "', TELEPHONE = '" + TELEPHONE + "', ADRESSE = '" + ADRESSE + "' " + where;
            MySqlCommand command = new MySqlCommand(queryString, connection);
            command.ExecuteNonQuery();
            this.closeConnect();
        }

        public void deleteClient(String ID)
        {
            this.openConnect();
            string where = "WHERE ID = '" + ID + "'";
            string queryString = "DELETE FROM client " + where ;
            MySqlCommand command = new MySqlCommand(queryString, connection);
            command.ExecuteNonQuery();
            this.closeConnect();
        }

        public void openConnect()
        {
            connection.Open();
        }

        public void closeConnect()
        {
            connection.Close();
        }

    }
}

