﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using NotreProjet;

namespace NotreProjet
{
    class Cnx_bdd
    {
        private static string connectionString = "SERVER=127.0.0.1; DATABASE=TP_FACTURATION; UID=root; PASSWORD=;";
        private static MySqlConnection connection = new MySqlConnection(connectionString);

        public List<Password> getAllPassword()
        {
            List<Password> mesPasswords = new List<Password>();
            string queryString = "SELECT * FROM ges_mdp";
            MySqlCommand command = new MySqlCommand(queryString, connection);
            this.openConnect();
            MySqlDataReader oReader = command.ExecuteReader();

            while (oReader.Read())
            {
                Password monPass = new Password();
                monPass.Id = oReader["MDP_ID"].ToString();
                monPass.Pseudo = oReader["MDP_LOGIN"].ToString();
                monPass.Site = oReader["MDP_SITE"].ToString();
                monPass.Mdp = oReader["MDP_PASSWORD"].ToString();
                mesPasswords.Add(monPass);
            }
            this.closeConnect();

            return mesPasswords;
        }

        public void addCompte(String Login, String password, String Site)
        {
            this.openConnect();
            string param = "('" + Login + "','" + password + "'," + "'emerickneel@gmail.com'" + ",'" + Site + "')";
            string queryString = "INSERT INTO ges_mdp (MDP_LOGIN,MDP_PASSWORD,MDP_EMAIL,MDP_SITE) VALUES" + param;
            MySqlCommand command = new MySqlCommand(queryString, connection);
            command.ExecuteNonQuery();
            this.closeConnect();
        }

        public void updateCompte(int Id, String Login, String password, String Site)
        {
            this.openConnect();
            string where = "WHERE MDP_ID = " + Id;
            string queryString = "UPDATE ges_mdp SET MDP_LOGIN = '" + Login + "', MDP_PASSWORD = '" + password + "', MDP_SITE = '" + Site + "' " + where;
            MySqlCommand command = new MySqlCommand(queryString, connection);
            this.closeConnect();
        }

        public void openConnect()
        {
            connection.Open();
        }

        public void closeConnect()
        {
            connection.Close();
        }

    }
}

